FROM debian:12

COPY scripts /scripts

RUN apt update && \
    apt dist-upgrade -y && \
    apt install handbrake-cli cron -y && \
    touch /tmp/encoder.log && \
    chmod +x /scripts/* && \
    echo "20 * * * * root nice -n 19 /scripts/dvd.sh >> /tmp/encoder.log" >> /etc/crontab && \
    echo "40 * * * * root nice -n 19 /scripts/br.sh >> /tmp/encoder.log" >> /etc/crontab

ENV DESTINATION "/data/completed/"
ENV SOURCE_BR "/data/incomplete/br/"
ENV SOURCE_DVD "/data/incomplete/dvd/"
ENV LOCK "/tmp/.encode"
ENV ENCODESTORAGE "/data/incomplete/encoding/"
ENV LANGUAGE "eng"
ENV EXT ".m4v"

CMD cron && tail -f /tmp/encoder.log