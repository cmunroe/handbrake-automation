# Handbrake Automation

https://www.youtube.com/watch?v=RZ8ijmy3qPo

## Goals

1. Automatic.
2. Multiple Release Options.
3. Simple and Cheap.
4. Profit?!

## Requirements

```
apt install handbrakecli 
```


## Links

https://gist.github.com/bashtheshell/2e3509e862e67112d7b0e123211caa3a

https://github.com/HandBrake/HandBrake-docs/blob/master/source/docs/en/latest/technical/official-presets.markdown

https://github.com/HandBrake/HandBrake-docs/blob/master/source/docs/en/latest/cli/command-line-reference.markdown
