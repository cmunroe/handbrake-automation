#!/bin/bash

PRESET="Apple 2160p60 4K HEVC Surround"
TAGS="[BluRay]"


if [[ -f ${LOCK} ]]; then
        echo "Already Running"
        exit 1
else
        touch ${LOCK}
fi

cd ${SOURCE_BR}

for MOVIE in *
do
        #echo ${MOVIE}

        SOURCEVIDEO=`find "${MOVIE}" -type f -mmin +60 -print`

        if [[ ${SOURCEVIDEO} != "" ]]; then
                echo ${SOURCEVIDEO}

                # Encode
                HandBrakeCLI \
                        --preset "${PRESET}" \
                        --input "${SOURCEVIDEO}" \
                        --audio-lang-list "${LANGUAGE}" \
                        --subtitle-lang-list "${LANGUAGE}" \
                        --subtitle-burned=none \
                        --output "${ENCODESTORAGE}${MOVIE}${TAGS}${EXT}"

                # Move Media
                mkdir "${DESTINATION}${MOVIE}/"
                mv "${ENCODESTORAGE}${MOVIE}${TAGS}${EXT}" "${DESTINATION}${MOVIE}/"

                # Remove Media
                rm -rf "${MOVIE}"
        fi
done

rm ${LOCK}
